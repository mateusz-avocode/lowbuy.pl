# lowbuy.pl - general info
		
### Branch name convention:
<change-type>/<feature-type>_<task-title>
		
EXAMPLE:  
feature/backlog_install-antD
		
### Commit name convention:  
<change-type>(<feature-type>): <task-title> . <developer-comment>
		
EXAMPLE:  
feature(backlog): Install antD
		
#### Change types
CHANGE_TYPE: feature / bugfix / hotfix / change