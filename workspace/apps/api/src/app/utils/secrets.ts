import dotenv from 'dotenv';
import * as fs from 'fs';

if (fs.existsSync('.env')) {
  dotenv.config({ path: '.env' });
}
else if (process.env.NODE_ENV !== 'production') {
  console.log('ENV file doesn\' exist' );
  process.exit();
}

export const ENVIRONMENT = process.env.NODE_ENV;
export const prod = ENVIRONMENT === 'production';
export const MONGODB_URI = process.env['MONGODB_URI'];
export const MONGODB_DB_NAME = process.env['MONGODB_DB_NAME']

if (!MONGODB_URI) {
  console.log('No mongo connection string. Set MONGODB_URI environment variable.');
  process.exit(1);
}
