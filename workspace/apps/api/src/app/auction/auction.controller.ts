import {Body, Controller, Get, Param, Post} from '@nestjs/common';
import {ApiOperation, ApiTags} from '@nestjs/swagger';
import {AddAuctionDTO} from "./dto/add-auction";
import {AuctionService} from "./auction.service";

@Controller('auction')
@ApiTags('Auction')
export class AuctionController {
  constructor(
    private auctionService: AuctionService
  ) { }

  @Get('/')
  @ApiOperation({description: 'All auctions'})
  public auctions() {
    return this.auctionService.auctions();
  }

  @Get('/:id')
  public auction(@Param('id') id: string) {
    return this.auctionService.getAuction(id);
  }

  @Post('/')
  public addAuction(@Body() addAuctionDTO: AddAuctionDTO) {
    return this.auctionService.addAuction(addAuctionDTO);
  }
}
