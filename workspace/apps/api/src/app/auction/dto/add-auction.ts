export interface AddAuctionDTO {
  title: string;
  description: string;
  product: object
}
