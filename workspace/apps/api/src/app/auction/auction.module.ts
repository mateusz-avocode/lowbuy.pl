import { Module } from '@nestjs/common';
import { AuctionController } from './auction.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {AuctionSchema} from "./schemas/auction.schema";
import {AuctionService} from "./auction.service";
import {ProductSchema} from "../product/schemas/product.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: 'Auction', schema: AuctionSchema},
      {name: 'Product', schema: ProductSchema}]),

  ],
  exports: [],
  controllers: [
    AuctionController
  ],
  providers: [
    AuctionService
  ]
})
export class AuctionModule {}
