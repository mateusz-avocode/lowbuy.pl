import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type AuctionDocument = Auction & Document;

@Schema()
export class Auction {
  public _id?: string;

  @Prop({ required: true })
  public title: string;

  @Prop({ required: true })
  public description: string;

  @Prop()
  public product: object;
}

export const AuctionSchema = SchemaFactory.createForClass(Auction);
