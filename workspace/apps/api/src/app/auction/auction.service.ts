import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Auction, AuctionDocument} from "./schemas/auction.schema";
import { AddAuctionDTO } from "./dto/add-auction";
import {ProductDocument} from "../product/schemas/product.schema";

@Injectable()
export class AuctionService {
  constructor(
    @InjectModel('Auction') private readonly auctionModel: Model<AuctionDocument>,
    @InjectModel('Product') private readonly productModel: Model<ProductDocument>
  ) { }

  public async addAuction (addAuctionDTO: AddAuctionDTO) {
    const auction = new this.auctionModel(addAuctionDTO);
    auction.product = await this.productModel.findById(auction.product);
    const d = await auction.save();
    return this.buildAuctionInfo(d);
  }

  public async getAuction(id) {
    const auction = await this.auctionModel.findById(id);
    return auction;
  }

  public async auctions() {
    return this.auctionModel.find();
  }

  private buildAuctionInfo(auction: AuctionDocument): Auction {
    return {
      _id: auction._id,
      title: auction.title,
      description: auction.description,
      product: auction.product
    }
  }
}
