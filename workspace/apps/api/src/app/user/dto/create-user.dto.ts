import { IsNotEmpty, MinLength, MaxLength, IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({
    example: 'Jon',
    description: 'The first name of the User',
    format: 'string',
  })
  @IsString()
  @IsNotEmpty()
  readonly firstName: string;

  @ApiProperty({
    example: 'Covalsky',
    description: 'The last name of the User',
    format: 'string',
  })
  @IsString()
  @IsNotEmpty()
  readonly lastName: string;
}
