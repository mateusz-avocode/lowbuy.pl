import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './schemas/user.schema';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UserService {

  constructor(
    @InjectModel('User') private readonly userModel: Model<UserDocument>
  ) {
  }

  async create(createUser: CreateUserDto) {
    const user = new this.userModel(createUser);
    await user.save();
    return this.buildCreateUserInfo(user);
  }

  private buildCreateUserInfo(user: UserDocument): User {
    const { firstName, lastName } = user;
    return { firstName, lastName };
  }
}
