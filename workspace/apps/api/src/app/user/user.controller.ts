import { Body, Controller, Get, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { CreateUserDto } from './dto/create-user.dto';

@Controller('user')
@ApiTags('User')
export class UserController {

  constructor(
    private readonly userService: UserService
  ) {
  }

  @Get('/')
  @ApiOperation({ description: 'All users' })
  public users() {
    return 'user'
  }

  @Post('/')
  @ApiOperation({ description: 'Create user' })
  public async createUser(
    @Body() createUser: CreateUserDto
  ) {
    return await this.userService.create(createUser);
  }
}
