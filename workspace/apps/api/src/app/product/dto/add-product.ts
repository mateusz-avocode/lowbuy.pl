export interface AddProductDTO {
  title: string;
  description: string;
}
