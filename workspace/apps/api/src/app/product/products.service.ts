import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Product, ProductDocument } from './schemas/product.schema';
import { AddProductDTO } from './dto/add-product';

@Injectable()
export class ProductsService {
  constructor(
    @InjectModel('Product') private readonly productModel: Model<ProductDocument>
  ) { }

  public async addProduct(addProductDTO: AddProductDTO) {
    const product = new this.productModel(addProductDTO);
    const d = await product.save();
    return this.buildProductInfo(d);
  }

  public async products() {
    return this.productModel.find();
  }

  public async getProductById(id) {
    const product = await this.productModel.findById(id);

    return product;
  }

  private buildProductInfo(product: ProductDocument): Product  {
    return {
      _id: product._id,
      title: product.title,
      description: product.description
    }
  }
}
