import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { AddProductDTO } from './dto/add-product';
import { ProductsService } from './products.service';

@Controller('product')
@ApiTags('Product')
export class ProductController {
  constructor(
    private productsService: ProductsService
  ) {}

  @Get('/')
  @ApiOperation({ description: 'All products' })
  public async product() {
    return this.productsService.products();
  }

  @Get('/:id')
  public productByID(@Param() id: string ) {
    return [].find(a => a._id === id);
  }

  @Post('/')
  public async addProduct(@Body() addProductDTO: AddProductDTO) {
    const p = await this.productsService.addProduct(addProductDTO);
    return p;
  }
}
