import { Test, TestingModule } from '@nestjs/testing';
import { ProductController } from './product.controller';
import { ProductsService } from './products.service';

describe('ProductController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [ProductController],
      providers: [ProductsService],
    }).compile();
  });

  it('should return all products', () => {
    const appController = app.get<ProductController>(ProductController);
    expect(true).toBe(true);
  });
});
