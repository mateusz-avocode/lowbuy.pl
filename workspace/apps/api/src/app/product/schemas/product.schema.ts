import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ProductDocument = Product & Document;

@Schema()
export class Product {
  public _id?: string;

  @Prop({ required: true })
  public title: string;

  @Prop({ required: true })
  public description: string;
}

export const ProductSchema = SchemaFactory.createForClass(Product);
