import { Controller, Get, Post } from '@nestjs/common';
import { Message } from '@workspace/api-interfaces';
import { AppService } from './app.service';

interface HelloDTO {
  message: string;
}

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService
  ) {}

  @Get('hello')
  getData(): HelloDTO {
    return {  message: 'hello world!'  };
  }
}
