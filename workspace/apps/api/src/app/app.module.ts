import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { MONGODB_URI } from './utils/secrets';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductModule } from './product/product.module';
import {AuctionModule} from "./auction/auction.module";

@Module({
  imports: [
    MongooseModule.forRoot(MONGODB_URI),
    UserModule,
    ProductModule,
    AuctionModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
