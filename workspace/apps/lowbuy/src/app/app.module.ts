import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { ThemeModule } from '@workspace/theme';
import {AuctionPdpComponent, AuctionsComponent, AuctionsModule} from '@workspace/auctions';
import { LayoutModule, MainLayoutComponent } from '@workspace/layout';
import { RouterModule, Routes } from '@angular/router';
import { HomePageModule, HomePageComponent } from '@workspace/home-page';

const routes: Routes = [
  {
    path: '', component: MainLayoutComponent, children: [
      { path: '', component: HomePageComponent },
      { path: 'auctions', component: AuctionsComponent },
      { path: 'auctions/:id', component: AuctionPdpComponent},
    ]
  }
]

@NgModule({
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    TransferHttpCacheModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    ThemeModule,
    AuctionsModule,
    LayoutModule,
    HomePageModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [
    AppComponent
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
