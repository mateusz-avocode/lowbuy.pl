import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";

interface Product {
  _id?: string;
  title: string,
  description: string,
  product: string,
}

@Component({
  selector: 'lowbuy-add-auction',
  templateUrl: './add-auction.component.html',
  styleUrls: ['./add-auction.component.css']
})
export class AddAuctionComponent implements OnInit {
  public form: FormGroup
  public products: Product[] = [];

  constructor(
    private fb: FormBuilder,
    private http: HttpClient
  ) { }

  public ngOnInit(): void {
    this.form = this.fb.group({
      title: [],
      description: [''],
      product: [''],
    });

    this.http.get<Product[]>('http://localhost:5000/api/product').subscribe(
      (response: Product[]) => {
        this.products = response;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  public onSubmit() {

    this.http.post('http://localhost:5000/api/auction/', this.form.value).subscribe(r => {
    })
  }

}
