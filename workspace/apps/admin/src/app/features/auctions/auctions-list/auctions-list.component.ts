import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'lowbuy-auctions-list',
  templateUrl: './auctions-list.component.html',
  styleUrls: ['./auctions-list.component.css']
})
export class AuctionsListComponent implements OnInit {

  @Input() public title;
  @Input() public description;
  @Input() public product;

  constructor() { }

  ngOnInit(): void {
  }

}
