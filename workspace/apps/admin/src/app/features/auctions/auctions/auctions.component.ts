import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

interface Auction {
  title: String,
  description: String,
  product: Object
}

@Component({
  selector: 'lowbuy-auctions',
  templateUrl: './auctions.component.html',
  styleUrls: ['./auctions.component.css']
})
export class AuctionsComponent implements OnInit {

  public auctions: Auction[] = [];

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit(): void {

    this.http.get<Auction[]>('http://localhost:5000/api/auction').subscribe(
      (response: Auction[]) => {
        console.log(response);
        this.auctions = response
      },
      (error) => {
        console.log(error);
      }
    )
  }

}
