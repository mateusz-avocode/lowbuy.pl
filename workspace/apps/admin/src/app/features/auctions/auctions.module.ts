import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuctionsComponent } from './auctions/auctions.component';
import { AddAuctionComponent } from './add-auction/add-auction.component';
import {RouterModule, Routes} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { AuctionsListComponent } from './auctions-list/auctions-list.component';

const routes: Routes = [
  {
    path: '',
    component: AuctionsComponent
  },
  {
    path: 'add-auction',
    component: AddAuctionComponent
  }

];

@NgModule({
  declarations: [AuctionsComponent, AddAuctionComponent, AuctionsListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],

})
export class AuctionsModule { }
