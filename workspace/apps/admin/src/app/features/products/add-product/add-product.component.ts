import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'lowbuy-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  public form: FormGroup

  constructor(
    private fb: FormBuilder,
    private http: HttpClient
  ) { }

  public ngOnInit(): void {
    this.form = this.fb.group({
      title: [],
      description: ['']
    });
  }

  public onSubmit() {
    console.log(this.form.value);
    this.http.post('http://localhost:5000/api/product/', this.form.value).subscribe(r => {
      console.log(r);
    })
  }
}
