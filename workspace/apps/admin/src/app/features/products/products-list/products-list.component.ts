import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'lowbuy-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  @Input() public title;
  @Input() public description;

  constructor(
  ) { }

  ngOnInit(): void {
  }
}
