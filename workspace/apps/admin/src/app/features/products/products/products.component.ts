import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

interface Product {
  title: string,
  description: string
}

@Component({
  selector: 'lowbuy-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public products: Product[] = [];

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.http.get<Product[]>('http://localhost:5000/api/product').subscribe(
      (response: Product[]) => {
        this.products = response;
      },
      (error) => {
        console.log(error);
      }
    )
  }
}
