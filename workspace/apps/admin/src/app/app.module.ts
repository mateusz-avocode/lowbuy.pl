import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './features/home-page/home-page/home-page.component';
import { HomePageModule } from './features/home-page/home-page.module';
import { DashboardModule } from './features/dashboard/dashboard.module';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./features/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'products',
    loadChildren: () => import('./features/products/products.module').then(m => m.ProductsModule)
  },
  {
    path: 'auctions',
    loadChildren: () => import('./features/auctions/auctions.module').then(m => m.AuctionsModule)
  }
]

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HomePageModule
  ],
  declarations: [AppComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
