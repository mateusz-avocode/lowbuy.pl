import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { ThemeModule } from '@workspace/theme';
import { RouterModule } from '@angular/router';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';

const components = [
  MainLayoutComponent,
  AdminLayoutComponent
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ThemeModule
  ],
  declarations: [...components],
  exports: [
    ...components
  ]
})
export class LayoutModule {
}
