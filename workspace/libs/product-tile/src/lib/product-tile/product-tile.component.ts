import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'lowbuy-product-tile',
  templateUrl: './product-tile.component.html',
  styleUrls: ['./product-tile.component.scss'],
})
export class ProductTileComponent implements OnInit {

  @Input() public id;
  @Input() public title;
  @Input() public product;
  @Input() public url;

  constructor() {
  }

  ngOnInit(): void {
  }
}
