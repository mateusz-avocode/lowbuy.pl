import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ProductTileComponent} from './product-tile/product-tile.component';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [ProductTileComponent],
  exports: [ProductTileComponent],
})
export class ProductTileModule {
}
