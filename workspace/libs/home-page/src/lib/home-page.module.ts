import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
import { HomeSliderComponent } from './home-slider/home-slider.component';
import { SliderModule } from "@workspace/slider";
import { ProductSliderModule } from '@workspace/product-slider';
import { HomeServiceComponent } from './home-service/home-service.component';
import { HomeParallaxBannerComponent } from './home-parallax-banner/home-parallax-banner.component';
import { ActionButtonModule } from "@workspace/ui";


@NgModule({
  imports: [
    CommonModule,
    ProductSliderModule,
    SliderModule,
  ],
  declarations: [
    HomePageComponent,
    HomeSliderComponent,
    HomeServiceComponent,
    HomeParallaxBannerComponent
  ],
  exports: [HomePageComponent]
})
export class HomePageModule {
}
