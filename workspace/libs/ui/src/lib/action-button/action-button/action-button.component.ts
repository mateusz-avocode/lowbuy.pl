import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'lowbuy-action-button',
  templateUrl: './action-button.component.html',
  styleUrls: ['./action-button.component.css'],
})
export class ActionButtonComponent implements OnInit, OnChanges {

  @Input() public disabled = false;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.disabled);
  }


  ngOnInit(): void {
  }
}
