import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ActivatedRoute} from "@angular/router";
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

interface Auction {
  title: String,
  description: String,
  product: Object
}

@Component({
  selector: 'lowbuy-auction-pdp',
  templateUrl: './auction-pdp.component.html',
  styleUrls: ['./auction-pdp.component.scss']
})
export class AuctionPdpComponent implements OnInit, OnDestroy {
  public auction$: Observable<Auction>;
  public auctionID$: Observable<string>;
  private routeSub: Subscription;
  public auctionTitle;
  public auctionDescription;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      const auctionID = params['id'];
      this.auction$ = this.http.get<Auction>(`http://localhost:5000/api/auction/${auctionID}`);
    });

    this.auctionID$ = this.route.params.pipe(
      map(p => p['id'])
    );
  }

  ngOnDestroy() {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }
}
