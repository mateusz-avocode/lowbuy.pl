import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionPdpComponent } from './auction-pdp.component';

describe('AuctionPdpComponent', () => {
  let component: AuctionPdpComponent;
  let fixture: ComponentFixture<AuctionPdpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuctionPdpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionPdpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
