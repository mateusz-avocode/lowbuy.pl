import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuctionsComponent } from './auctions/auctions.component';
import { HttpClientModule } from '@angular/common/http';
import {ProductTileModule} from "@workspace/product-tile";
import {SliderModule} from "@workspace/slider";
import { AuctionPdpComponent } from './auction-pdp/auction-pdp.component';
const components = [
  AuctionsComponent,
  AuctionPdpComponent
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    SliderModule,
    ProductTileModule
  ],
  declarations: [AuctionsComponent, AuctionPdpComponent],
  exports: [
    ...components
  ]
})
export class AuctionsModule {}
