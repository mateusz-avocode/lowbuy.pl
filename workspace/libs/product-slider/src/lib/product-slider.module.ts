import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductSliderComponent} from './product-slider/product-slider.component';
import {ProductTileModule} from "@workspace/product-tile";

@NgModule({
  imports: [CommonModule, ProductTileModule],
  declarations: [ProductSliderComponent],
  exports: [ProductSliderComponent]
})
export class ProductSliderModule {
}
