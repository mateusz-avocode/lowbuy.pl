def projectName = "lowbuy.pl"

pipeline {
    agent any

    stages {
        stage('Build staging app image') {
           when {
               expression { GIT_BRANCH == 'origin/develop' }
           }
            steps {
               sh "docker build -t ${projectName} /var/lib/jenkins/workspace/${projectName}/workspace --build-arg ENV=staging"
            }
       }
       stage('Build production app image') {
           when {
               expression { GIT_BRANCH == 'origin/master' }
           }
           steps {
              sh "docker build -t ${projectName} /var/lib/jenkins/workspace/${projectName}/workspace --build-arg ENV=prod"
           }
      }
        stage('Delete old staging containers') {
            when {
                expression { GIT_BRANCH == 'origin/develop' }
            }
            steps {
               sh "docker stop ${projectName}-backend-staging || true"
               sh "docker stop ${projectName}-frontend-staging || true"
               sh "docker rm ${projectName}-backend-staging || true"
               sh "docker rm ${projectName}-frontend-staging || true"
            }
        }
        stage('Delete old production containers') {
            when {
                expression { GIT_BRANCH == 'origin/master' }
            }
            steps {
               sh "docker stop ${projectName}-backend-prod || true"
               sh "docker stop ${projectName}-frontend-prod || true"
               sh "docker rm ${projectName}-backend-prod || true"
               sh "docker rm ${projectName}-frontend-prod || true"
            }
        }
        stage('Run staging frontend containers') {
            when {
                expression { GIT_BRANCH == 'origin/develop' }
            }
            steps {
               sh "docker run --name ${projectName}-frontend-staging -d -p 4010:4000 ${projectName} /bin/sh -c 'npm run serve:ssr:staging'"
            }
        }
        stage('Run production frontend containers') {
            when {
               expression { GIT_BRANCH == 'origin/master' }
            }
            steps {
               sh "docker run --name ${projectName}-frontend-prod -d -p 4000:4000 ${projectName} /bin/sh -c 'npm run serve:ssr'"
            }
        }

        stage('Run staging backend containers') {
            when {
               expression { GIT_BRANCH == 'origin/develop' }
            }
            steps {
               sh "docker run --name ${projectName}-backend-staging -d -p 5010:5000 ${projectName} /bin/sh -c 'npm run start:backend'"
            }
        }

        stage('Run production backend containers') {
            when {
               expression { GIT_BRANCH == 'origin/master' }
            }
            steps {
               sh "docker run --name ${projectName}-backend-prod -d -p 5000:5000 ${projectName} /bin/sh -c 'npm run start:backend'"
            }
        }
    }
}
